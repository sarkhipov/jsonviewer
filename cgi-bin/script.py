#!/usr/bin/python

import cgi, json, os.path, glob, ntpath


def getParam(name):
	return cgi.FieldStorage().getvalue(name)

def response(result, data):
	print "Content-type: text/json\n"
	print json.dumps({"result": result, "data": data})

def getConfig():
	file = open(os.path.abspath(os.path.dirname(__file__)) + '/../config', "r")
	path = file.read().strip()
	file.close()
	filesList = []
	for i in glob.glob(path + "*.json"):
		filesList.append(ntpath.basename(i))
	return {'path': path, 'files': sorted(filesList)}

def getConfigPath():
	config = getConfig()
	return config.get('path')

def getFile(file):
	file = getConfigPath() + file
	if os.path.exists(file) is False:
		response(0, "File does not exist")
		return
	file = open(file, "r")
	data = file.read()
	file.close()
	response(1, data)
	return

def saveFile(file, data):
	file = getConfigPath() + file
	if os.path.exists(file) is False:
		response(0, "Save error: file does not exist")
		return
	file = open(file, 'w')
	file.write(data)
	file.close()
	response(1, "File saved")

method = getParam('method')
if method == 'getFile':
	file = getParam('file').strip()
	getFile(file)

if method == 'saveFile':
	file = getParam('file').strip()
	saveFile(file, getParam('data').strip())

if method == 'getConfig':
	response(1, getConfig())

