var script = (function () {

    var field, baseField,
        wrap,
        currentFile, currentData,
        btnSave, loader,
        isSaved = true,
        errorClass = 'is-invalid',
        apiUrl = "/cgi-bin/script.py",
        files = [];

    /**
     *
     */
    function init() {
        field = $('.file-path');
        baseField = $('.base-dir');
        wrap = $('.content');
        btnSave = $('.save');
        loader = $('.loader');
        bind();

        setAutocomplete();
        setFileFromRequest();
        getConfig();
    }

    /**
     *
     */
    function bind() {
        $('.show-file').click(function () {
            getFile();
        });
        field.keyup(function (e) {
            clearErrors();
            if (e.keyCode == 13) {
                getFile();
            }
        });
        btnSave.click(function () {
            save();
        });
        $(document).on('keyup', '.data-item', function () {
            handleIsSaved(false);
        }).on('click', '.sortable', function () {
            sort($(this));
        });
    }

    /**
     *
     */
    function setAutocomplete() {
        field.autocomplete({
            source: function (request, response) {
                response(files);
            },
            minLength: 0,
            select: function() {
                setTimeout(function () {
                    getFile()
                }, 0);
            }
        }).focus(function(){
            $(this).data("uiAutocomplete").search($(this).val());
        });
    }

    /**
     *
     */
    function getConfig() {
        $.get(apiUrl, {method: 'getConfig'}, function (r) {
            baseField.val(r.data.path);
            files = r.data.files;
        });
    }

    /**
     *
     */
    function setFileFromRequest() {
        var file = getQueryParam('file');
        if (file) {
            field.val(file);
            getFile();
        }
    }

    /**
     *
     */
    function getFile() {
        if (!validate()) {
            return;
        }

        //
        if (!isSaved) {
            confirm('Data is not saved. Load a new file?', function () {
                showFile();
            }, function () {
                field.val(currentFile);
            });
            return;
        }
        //
        showFile();
    }

    /**
     * @returns {boolean}
     */
    function validate() {
        clearErrors();
        if (!field.val()) {
            field.addClass(errorClass);
            return false;
        }
        return true;
    }

    /**
     *
     */
    function showFile() {
        var file = field.val().replace('/', '');

        clearContent();
        addFileToUrl(file);
        field.trigger('blur');
        //
        loader.show();
        $.get(apiUrl, {method: 'getFile', file: file}, function(r) {
            loader.hide();
            if (!r.result) {
                alert(r.data, 'warning');
                return
            }
            currentFile = file;
            currentData = JSON.parse(r.data);
            //
            draw(true);
        });
    }

    /**
     *
     */
    function clearErrors() {
        field.removeClass(errorClass);
    }

    /**
     *
     */
    function clearContent() {
        wrap.html('');
        btnSave.hide();
        handleIsSaved(true);
    }

    /**
     * data
     */
    function draw(full) {
        if (full) {
            btnSave.show();
            handleIsSaved(true);
            var html = _.template($('#tpl-table').html(), currentData)(currentData);
            wrap.html(html);
        }
        var items = _.template($('#tpl-table-items').html(), currentData)(currentData);
        $('.items').html(items);
    }

    /**
     * @param saved
     */
    function handleIsSaved(saved) {
        btnSave.attr('disabled', saved ? 'disabled' : false);
        isSaved = saved;
    }

    /**
     *
     */
    function save() {
        handleIsSaved(true);
        var data = collectData();
        //
        loader.show();
        $.get(apiUrl, {method: 'saveFile', file: currentFile, data: JSON.stringify(data)}, function(r) {
            loader.hide();
            alert(r.data, r.result ? 'info' : 'warning');
        });
    }

    /**
     *
     */
    function collectData() {
        var data = {
            version: $('.version').text() * 1,
            observations: []
        };
        //
        $('.data-row').each(function () {
            var expData = $('.experiments', $(this)).text().split(','), exp = [];
            _.each(expData, function (item) {
                exp.push({testid: item.trim()});
            });
            var tagsData = $('.tags', $(this)).text().split(','), tags = [];
            _.each(tagsData, function (item) {
                tags.push(item.trim());
            });
            data.observations.push({
                observation_id: $('.id', $(this)).text(),
                date_from: $('.date_from', $(this)).text(),
                date_to: $('.date_to', $(this)).text(),
                control: {testid: $('.control', $(this)).text()},
                experiments: exp,
                tags: tags
            });
        });
        //
        return data;
    }

    /**
     * @param elem
     */
    function sort(elem) {
        $('.sortable.active').removeClass('active').find('.sort-arrow').remove();
        //
        var prevOrder = elem.data('order') || 'asc';
        var newOrder = prevOrder == 'asc' ? 'desc' : 'asc';
        elem.data('order', newOrder);
        elem.addClass('active').addClass(newOrder).removeClass(prevOrder);
        //
        var icons = {'asc': 'arrow-dropup', 'desc': 'arrow-dropdown'};
        elem.append('<ion-icon class="sort-arrow" name="'+icons[newOrder]+'"></ion-icon>');
        //
        sortData(elem.data('field'), newOrder);
    }

    /**
     * @param field
     * @param order
     */
    function sortData(field, order) {
        currentData.observations = _.sortBy(currentData.observations, function (o) {
            var f = field.split('.');
            return f.length == 1 ? o[field] : o[f[0]][f[1]];
        });
        if (order == 'desc') {
            currentData.observations.reverse();
        }
        draw();
    }

    /**
     * @param message
     * @param type
     */
    function alert(message, type) {
        $.notify({message: message}, {type: type, delay: 2000, template:
        '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0} alert-minimalist" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="position: absolute; right: 10px; top: 5px; z-index: 1033;">×</button>' +
        '<img data-notify="icon" class="img-circle pull-left">' +
        '<span data-notify="title">{1}</span>' +
        '<span data-notify="message">{2}</span>' +
        '</div>'
        });
    }

    /**
     * @param message
     * @param yesCallback
     * @param noCallback
     */
    function confirm(message, yesCallback, noCallback) {
        var modal = $('#modal-confirm');
        modal.find('.modal-body').html(message);
        modal.find('.confirm-yes').unbind('click')
            .click(function() {
                modal.modal('hide');
                if(yesCallback) {
                    yesCallback();
                }
            });
        modal.find('.confirm-no').unbind('click')
            .click(function() {
                modal.modal('hide');
                if(noCallback) {
                    noCallback();
                }
            });
        modal.modal('show');
    }

    /**
     * @param file
     */
    function addFileToUrl(file) {
        var url = window.location.href.split('?')[0];
        url += '?file=' + file;
        window.history.pushState({path: url}, '', url);
    }

    /**
     * @param param
     * @returns {*}
     */
    function getQueryParam(param) {
        var urlParams = new URLSearchParams(window.location.search);
        return urlParams.get(param);
    }

    //
    $(document).ready(function () {
        init();
    });

    return {};
})();
